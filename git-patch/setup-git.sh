#!/usr/bin/env bash

set -e

if [ -z "$CONTRA_CLUSTER_DEPLOY_KEY" ]; then
  echo 'ERROR: $CONTRA_CLUSTER_DEPLOY_KEY UNSET';
  exit 1;
fi;

mkdir -p /root/.ssh
cp $CONTRA_CLUSTER_DEPLOY_KEY /root/.ssh/id_rsa
ssh-keyscan -H gitlab.com > /root/.ssh/known_hosts
chmod 600 /root/.ssh/id_rsa

git config --global user.email 'ci@contra.com'
git config --global user.name 'GitLab CI/CD'