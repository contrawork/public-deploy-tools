#!/usr/bin/env bash

set -e

if [ -z "$CI_ENVIRONMENT_SLUG" ]; then
  echo 'ERROR: $CI_ENVIRONMENT_SLUG UNSET';
  exit 1;
fi;

# `sed "s/(stdin)= //"` is needed because depending on the openssl version it may be included in the output.
# `'s/[^a-z0-9]/p/g'` is needed to replace '+' with a URL safe alternative ('p' is chosen at random).
# 'c' prefix is added to prevent numbers-only deployment slug. Numbers only slug is converted to scientific notation (bug in Helm https://github.com/helm/helm/issues/1707).
export DEPLOYMENT_SLUG=c$(echo $CI_ENVIRONMENT_SLUG | openssl dgst -sha1 | sed "s/(stdin)= //" | cut -b1-7 | sed 's/[^a-z0-9]/p/g')