# Public Deploy Tools

Tools that need to be public for the consumption by Contra's distributed system.

## `build` image

Includes binaries:

* `docker`
* `gcloud`

## `git-patch` image

### `setup-git` program

Configures `git` using `$CONTRA_CLUSTER_DEPLOY_KEY`.

### `generate-deployment-slug` program

Populates `$DEPLOYMENT_SLUG` variable with a hash derived from `$CI_ENVIRONMENT_SLUG` value.