#!/usr/bin/env bash

set -e

if [ -z "$CI_PROJECT_NAME" ]; then
  echo 'ERROR: $CI_PROJECT_NAME UNSET';
  exit 1;
fi;

PROJECT_NAME="$CI_PROJECT_NAME"
REVIEW_PROJECT_NAME="$CI_PROJECT_NAME-review"

setup-git
source generate-deployment-slug

MANIFEST_PATH="applications/mother/templates/$REVIEW_PROJECT_NAME-$DEPLOYMENT_SLUG.yaml"

rm -rf contra-cluster
git clone --single-branch --branch master git@gitlab.com:contrawork/contra-cluster.git contra-cluster

cd ./contra-cluster

if [[ -f "$MANIFEST_PATH" ]]; then
    rm $MANIFEST_PATH

    git add -A

    git commit -am "[skip ci] remove $PROJECT_NAME $DEPLOYMENT_SLUG review app"
    git push origin master
fi
