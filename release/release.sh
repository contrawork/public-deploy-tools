#!/usr/bin/env bash

set -e

if [ -z "$CI_COMMIT_SHORT_SHA" ]; then
  echo 'ERROR: $CI_COMMIT_SHORT_SHA UNSET';
  exit 1;
fi;

if [ -z "$CI_PROJECT_NAME" ]; then
  echo 'ERROR: $CI_PROJECT_NAME UNSET';
  exit 1;
fi;

if [ -z "$RELEASE_TYPE" ]; then
  echo 'ERROR: $RELEASE_TYPE UNSET';
  exit 1;
fi;


IMAGE_TAG="$CI_COMMIT_SHORT_SHA"
PROJECT_NAME="$CI_PROJECT_NAME"
REVIEW_PROJECT_NAME="$CI_PROJECT_NAME-review"
CLUSTER_BRANCH_NAME="master" # Currently only master is supported

setup-git

rm -rf contra-cluster
git clone --single-branch --branch $CLUSTER_BRANCH_NAME git@gitlab.com:contrawork/contra-cluster.git contra-cluster

cd ./contra-cluster

if [ "$RELEASE_TYPE" = "review" ]; then
    source generate-deployment-slug

    MANIFEST_PATH="applications/mother/templates/$REVIEW_PROJECT_NAME-$DEPLOYMENT_SLUG.yaml"

    cat ../bin/$REVIEW_PROJECT_NAME-template.yaml | \
    sed "s/\$IMAGE_TAG/$IMAGE_TAG/" | \
    sed "s/\$DEPLOYMENT_VERSION/$IMAGE_TAG/" | \
    sed "s/\$DEPLOYMENT_SLUG/$DEPLOYMENT_SLUG/" > $MANIFEST_PATH

    git add $MANIFEST_PATH

    echo "DYNAMIC_ENVIRONMENT_URL=https://$ROOT_SLUG-$DEPLOYMENT_SLUG.contra.dev" >> ../deploy.env

    git commit -am "[skip ci] replace $PROJECT_NAME $DEPLOYMENT_SLUG review app"

    cat ../deploy.env

elif [ "$RELEASE_TYPE" = "production" ]; then
    yq eval '.image.tag = "'$IMAGE_TAG'"' -i applications/$PROJECT_NAME/values-production.yaml
    yq eval '.app.deployment.version = "'$IMAGE_TAG'"' -i applications/$PROJECT_NAME/values-production.yaml
    git commit -am "[skip ci] update $PROJECT_NAME production image.tag"
    
else
  echo "ERROR: unknown RELEASE_TYPE '$RELEASE_TYPE'";
  exit 1;
fi;


git push origin $CLUSTER_BRANCH_NAME
